#!/usr/bin/env python
# coding: utf-8
#Author: Martine Akossi
# In[64]:


import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


# In[65]:


class Plots_save_folder:
    '''
    import Define_folder_Plots as DFP
    Example:
    facet = sns.lmplot(data=df, x='Zeit', y='Leistung2', hue='label', 
                   fit_reg=False, legend=True, legend_out=True)

    facet.savefig(DFP.Plots_save_folder.mkdir_p('test2','test3'))
    '''
    
        def mkdir_p(mypath,file):
            '''Creates a directory. equivalent to using mkdir -p on the command line'''

            from errno import EEXIST
            from os import makedirs,path
            t = '{}/'
            Name = (t+file).format(mypath)
            try:
                makedirs(mypath)
            except OSError as exc: # Python >2.5
                if exc.errno == EEXIST and path.isdir(mypath):
                    pass
                else: raise
        
            
           
            
            
            return Name



